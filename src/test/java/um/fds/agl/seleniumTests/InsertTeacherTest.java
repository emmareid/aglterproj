package um.fds.agl.seleniumTests;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

public class InsertTeacherTest extends BaseForTests{

    @Test
    void insertTeacherByTeacherError() {
        login("Turing", "turing");
        
        WebElement teacherList = driver.findElement(By.linkText("Teachers List"));
        click(teacherList);

        String teacherListRef = driver.findElement(By.xpath("//table/tbody")).getText();

        WebElement addTeacher = driver.findElement(By.linkText("Add Teachers"));
        click(addTeacher);

        assertTrue(driver.getTitle().contains("Error"));

        WebElement returnHome = driver.findElement(By.linkText("Go back to home page"));
        click(returnHome);

        teacherList = driver.findElement(By.linkText("Teachers List"));
        click(teacherList);

        assertEquals(teacherListRef, driver.findElement(By.xpath("//table/tbody")).getText());
    }

    @Test
    void insertTeacherByTERManagerSuccess() throws IOException {
        login("Chef", "mdp");
        
        WebElement teacherList = driver.findElement(By.linkText("Teachers List"));
        click(teacherList);

        WebElement addTeacher = driver.findElement(By.linkText("Add Teachers"));
        click(addTeacher);

        WebElement firstname = driver.findElement(By.id("firstName"));
        WebElement lastname = driver.findElement(By.id("lastName"));
        WebElement submit = driver.findElement(By.cssSelector("[type=submit]"));
        write(firstname, "TeacherfirstnameforTest");
        write(lastname, "TeacherLastNameForTest");
        click(submit);

        assertTrue(driver.getTitle().contains("Teacher List"));

        assertTrue(driver.findElement(By.tagName("body")).getText().contains("TeacherLastNameForTest"));
        
        screenshot("screenshot_Test");
    }
}
