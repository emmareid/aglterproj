package um.fds.agl.ter22;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import um.fds.agl.ter22.entities.*;
import um.fds.agl.ter22.repositories.*;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final TeacherRepository teachers;
    
    private final TERManagerRepository managers;

    private final StudentRepository students;

    private final SubjectRepository subjects;


    @Autowired
    public DatabaseLoader(TeacherRepository teachers, TERManagerRepository managers, StudentRepository students, SubjectRepository subjects) {
        this.teachers = teachers;
        this.managers = managers;
        this.students = students;
        this.subjects = subjects;
    }

    @Override
    public void run(String... strings) throws Exception {
        TERManager terM1Manager=this.managers.save(new TERManager("Le","Chef", "mdp", RoleType.ROLE_MANAGER));
       SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Chef", "bigre",
                        AuthorityUtils.createAuthorityList(RoleType.ROLE_MANAGER))); // the actual password is not needed here
        this.teachers.save(new Teacher("Ada", "Lovelace", "lovelace",terM1Manager, RoleType.ROLE_TEACHER));
        this.teachers.save(new Teacher("Alan", "Turing", "turing",terM1Manager, RoleType.ROLE_TEACHER));
        this.teachers.save(new Teacher("Leslie", "Lamport", "lamport",terM1Manager, RoleType.ROLE_TEACHER));
        this.students.save(new Student("Gustave", "Flaubert"));
        this.students.save(new Student("Frédéric", "Chopin"));

        List<Teacher> secondaryInCharge = new ArrayList<>();
        secondaryInCharge.add(this.teachers.findByLastName("Lovelace"));
        secondaryInCharge.add(this.teachers.findByLastName("Lamport"));

        this.subjects.save(new SubjectTER("Subject 1", "Simple description of subject number 1", this.teachers.findByLastName("Turing"), secondaryInCharge));

        SecurityContextHolder.clearContext();

    }
}
