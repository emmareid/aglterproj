package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.GroupTER;
import um.fds.agl.ter22.repositories.GroupRepository;

import java.util.Optional;


@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    public Optional<GroupTER> getGroup(final Long id) {
        return groupRepository.findById(id);
    }

    public Iterable<GroupTER> getGroups() {
        return groupRepository.findAll();
    }

    public void deleteGroup(final Long id) {
        groupRepository.deleteById(id);
    }

    public GroupTER saveGroup(GroupTER group) {
        GroupTER savedStudent = groupRepository.save(group);
        return savedStudent;
    }

    public Optional<GroupTER> findById(long id) {
        return groupRepository.findById(id);
    }
}