package um.fds.agl.ter22.entities;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import um.fds.agl.ter22.RoleType;

@Entity
public class Student extends UserTER{

    private @ManyToOne GroupTER group;

    public Student(String firstName, String lastName){
        super(firstName, lastName);
        String[] roles={RoleType.ROLE_STUDENT};
        this.setRoles(roles);
    }

    public Student(long id, String firstName, String lastName) {
        super(id, firstName, lastName);
        String[] roles = {RoleType.ROLE_STUDENT};
        this.setRoles(roles);
    }

    public Student(String firstName, String lastName, GroupTER group){
        super(firstName, lastName);
        String[] roles={RoleType.ROLE_STUDENT};
        this.setRoles(roles);
        this.group = group;
    }

    public Student(long id, String firstName, String lastName, GroupTER group){
        super(id, firstName, lastName);
        String[] roles={RoleType.ROLE_STUDENT};
        this.setRoles(roles);
        this.group = group;
    }

    public Student() {}

    public void setGroup(GroupTER group){
        this.group = group;
    }

    public GroupTER getGroup(){
        return this.group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student user = (Student) o;

        if(getId()==null || user.getId()==null) return false;
        if (!getId().equals(user.getId())) return false;
        if (!getFirstName().equals(user.getFirstName())) return false;
        if (!getLastName().equals(user.getLastName())) return false;
        if (!getGroup().equals(user.getGroup())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(getRoles(), user.getRoles());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getFirstName().hashCode();
        result = 31 * result + getLastName().hashCode();
        result = 31 * result + getGroup().hashCode();
        result = 31 * result + Arrays.hashCode(getRoles());
        return result;
    }
}
