package um.fds.agl.ter22.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class GroupTER {
    private @OneToMany(mappedBy = "group") List<Student> studentList;
    private @Id    @GeneratedValue Long id;
    private String groupName = "Default Group Name";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        if(groupName.isBlank()) return;
        this.groupName = groupName;
    }
    
    public List<Student> getStudentList(){
        return studentList;
    }

    public void setStudentList(List<Student> studentList){
        this.studentList = studentList;
    }

    public GroupTER(){}

    public GroupTER(String groupName) {
        if(!(groupName.isBlank()))
            this.groupName = groupName;

        this.studentList = new ArrayList<>();
    }

    public GroupTER(String groupName, List<Student> studentList) {
        if(!(groupName.isBlank()))
            this.groupName = groupName;

        this.studentList = studentList;
    }

    public GroupTER(long id, String groupName) {
        this(groupName);
        this.id = id;
    }

    public GroupTER(long id, String groupName, List<Student> studentList) {
        this(groupName, studentList);
        this.id = id;
    }

    public String toString() {
        return "Group{" +
                "id=" + getId() +
                ", groupName='" + getGroupName() + '\'' +
                ", studentList='" + getStudentList() + '\'' +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupTER)) return false;

        GroupTER subject = (GroupTER) o;

        if (!getId().equals(subject.getId())) return false;
        if (!getGroupName().equals(subject.getGroupName())) return false;
        return getStudentList().equals(subject.getStudentList());
    }

    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getGroupName().hashCode();
        result = 31 * result + getStudentList().hashCode();
        return result;
    }
}
