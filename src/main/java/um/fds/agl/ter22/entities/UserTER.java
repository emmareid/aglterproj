package um.fds.agl.ter22.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import java.util.Arrays;
import java.util.List;


@Entity
@Inheritance
public abstract class UserTER {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Id
    @GenericGenerator(name = "MyGenerator", strategy = "um.fds.agl.ter22.MyGenerator")
    @GeneratedValue(generator = "MyGenerator")
    @Column(unique = true, nullable = false)
    private Long id;

    private @OneToMany(mappedBy = "userInCharge") List<SubjectTER> subjectsInCharge;
    private @ManyToMany(mappedBy = "secondaryInCharge") List<SubjectTER> subjectsSecondary;

    private String firstName = "John";
    private String lastName = "Doe";
    
    private @JsonIgnore String password;
    private @JsonIgnore String[] roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(firstName == null || firstName.isBlank()) return;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if(lastName == null || lastName.isBlank()) return;
        this.lastName = lastName;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    protected UserTER(){}

    protected UserTER(String firstName, String lastName, String password, String[] roles) {
        if(!(firstName == null || firstName.isBlank()))
            this.firstName = firstName;
            
        if(!(lastName == null || lastName.isBlank()))
            this.lastName = lastName;

        setPassword(password);
        this.roles = roles;
    }

    protected UserTER(String firstName, String lastName) {
        //default : the password is the name, no role ...
        this(firstName, lastName, lastName, new String[0]);
    }

    protected UserTER(long id, String firstName, String lastName) {
        //default : the password is the name, no role ...
        this(firstName, lastName, lastName, new String[0]);
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserTER)) return false;

        UserTER user = (UserTER) o;

        if(getId()==null || user.getId()==null) return false;
        if (!getId().equals(user.getId())) return false;
        if (!getFirstName().equals(user.getFirstName())) return false;
        if (!getLastName().equals(user.getLastName())) return false;
        if (!password.equals(user.password)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getFirstName().hashCode();
        result = 31 * result + getLastName().hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + Arrays.hashCode(roles);
        return result;
    }
}
