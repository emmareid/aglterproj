package um.fds.agl.ter22.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class SubjectTER {
    private @ManyToOne UserTER userInCharge;
    private @ManyToMany List<Teacher> secondaryInCharge;
    private @Id    @GeneratedValue Long id;
    private String subjectName = "Default Subject Name";
    private String subjectDescription = "Default Subject Description";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        if(subjectName.isBlank() || subjectName == null) return;
        this.subjectName = subjectName;
    }
    
    public String getSubjectDescription() {
        return subjectDescription;
    }

    public void setSubjectDescription(String subjectDescription) {
        if(subjectDescription.isBlank() || subjectDescription == null) return;
        this.subjectDescription = subjectDescription;
    }

    public UserTER getUserInCharge() {
        return userInCharge;
    }

    public void setUserInCharge(UserTER userInCharge) {
        this.userInCharge = userInCharge;
    }

    public List<Teacher> getSecondaryInCharge(){
        return secondaryInCharge;
    }

    public void setSecondaryInCharge(List<Teacher> secondaryInCharge){
        this.secondaryInCharge = secondaryInCharge;
    }

    public SubjectTER(){}

    public SubjectTER(String subjectName, String subjectDescription, UserTER userInCharge) {
        if(!(subjectName.isBlank() || subjectName == null))
            this.subjectName = subjectName;

        if(!(subjectDescription.isBlank() || subjectDescription == null))
            this.subjectDescription = subjectDescription;

        this.userInCharge = userInCharge;
        this.secondaryInCharge = new ArrayList<Teacher>();
    }

    public SubjectTER(String subjectName, String subjectDescription, UserTER userInCharge, List<Teacher> secondaryInCharge) {
        if(!(subjectName.isBlank() || subjectName == null))
            this.subjectName = subjectName;

        if(!(subjectDescription.isBlank() || subjectDescription == null))
            this.subjectDescription = subjectDescription;

        this.userInCharge = userInCharge;
        this.secondaryInCharge = secondaryInCharge;
    }

    public SubjectTER(long id, String subjectName, String subjectDescription, UserTER userInCharge) {
        this(subjectName, subjectDescription, userInCharge);
        this.id = id;
    }

    public SubjectTER(long id, String subjectName, String subjectDescription, UserTER userInCharge, List<Teacher> secondaryInCharge) {
        this(subjectName, subjectDescription, userInCharge, secondaryInCharge);
        this.id = id;
    }

    public String toString() {
        return "Subject{" +
                "id=" + getId() +
                ", subjectName='" + getSubjectName() + '\'' +
                ", subjectDescription='" + getSubjectDescription() + '\'' +
                ", userInCharge='" + getUserInCharge() + '\'' +
                ", secondaryInCharge='" + getSecondaryInCharge() + '\'' +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubjectTER)) return false;

        SubjectTER subject = (SubjectTER) o;

        if (!getId().equals(subject.getId())) return false;
        if (!getSubjectName().equals(subject.getSubjectName())) return false;
        if (!getSubjectDescription().equals(subject.getSubjectDescription())) return false;
        if (!getSecondaryInCharge().equals(subject.getSecondaryInCharge())) return false;
        return getUserInCharge().equals(subject.getUserInCharge());
    }

    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getSubjectName().hashCode();
        result = 31 * result + getSubjectDescription().hashCode();
        result = 31 * result + getSecondaryInCharge().hashCode();
        result = 31 * result + getUserInCharge().hashCode();
        return result;
    }
}
