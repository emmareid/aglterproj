package um.fds.agl.ter22.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import um.fds.agl.ter22.entities.GroupTER;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.forms.GroupForm;
import um.fds.agl.ter22.repositories.StudentRepository;
import um.fds.agl.ter22.services.GroupService;

@Controller
public class GroupController implements ErrorController {

    public static final String REDIRECT_GROUP = "redirect:/listGroups";

    @Autowired
    private GroupService groupService;
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/listGroups")
    public Iterable<GroupTER> getGroups(Model model) {
        model.addAttribute("groups", groupService.getGroups());
        return groupService.getGroups();
    }

    @ModelAttribute("currentUser")
    public Student currentUserTER(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return studentRepository.findByLastName(userDetails.getUsername());
    }
    
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = { "/createGroup" })
    public String showCreateGroup(Model model) {

        GroupForm groupForm = new GroupForm();
        model.addAttribute("groupForm", groupForm);

        return "createGroup";
    }

    @PostMapping(value = { "/createGroup"})
    public String createGroup(Model model, @ModelAttribute("GroupForm") GroupForm groupForm) {
        GroupTER group;
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Student student = studentRepository.findByLastName(userDetails.getUsername());

        group=new GroupTER(groupForm.getGroupName());
        if(student != null){
            student.setGroup(group);
            group.getStudentList().add(student);
        }
        groupService.saveGroup(group);

        return REDIRECT_GROUP;
    }

    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @RequestMapping(value = "/listGroups", params = {"subscribeGroup"})
    public String subscribeGroup(Model model, final HttpServletRequest req){
        final Integer groupId = Integer.valueOf(req.getParameter("subscribeGroup"));
        GroupTER group = groupService.findById(groupId).get();

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student student = studentRepository.findByLastName(userDetails.getUsername());

        student.setGroup(group);
        group.getStudentList().add(student);
        
        groupService.saveGroup(group);
        
        return REDIRECT_GROUP;
    }

    @PreAuthorize("hasRole('ROLE_STUDENT')")
    @RequestMapping(value = "/listGroups", params = {"unsubscribeGroup"})
    public String unsubscribeGroup(Model model, final HttpServletRequest req){
        final Integer groupId = Integer.valueOf(req.getParameter("unsubscribeGroup"));
        GroupTER group = groupService.findById(groupId).get();

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student student = studentRepository.findByLastName(userDetails.getUsername());
        
        student.setGroup(null);
        group.getStudentList().remove(student);
        
        groupService.saveGroup(group);
        
        return REDIRECT_GROUP;
    }

    @GetMapping(value = {"/deleteGroup/{id}"})
    public String deleteGroup(Model model, @PathVariable(value = "id") long id){
        groupService.deleteGroup(id);
        return REDIRECT_GROUP;
    }
}
