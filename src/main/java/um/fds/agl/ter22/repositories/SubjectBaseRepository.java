package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import um.fds.agl.ter22.entities.SubjectTER;

@NoRepositoryBean
public interface SubjectBaseRepository<T extends SubjectTER>
        extends CrudRepository<T, Long> {

    public T findByUserInCharge(String userInCharge);

}
