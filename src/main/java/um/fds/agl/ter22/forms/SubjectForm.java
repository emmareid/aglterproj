package um.fds.agl.ter22.forms;

import java.util.ArrayList;
import java.util.List;

import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.UserTER;

public class SubjectForm {
    private String subjectName;
    private String subjectDescription;
    private UserTER userInCharge;
    private List<Teacher> secondaryInCharge = new ArrayList<Teacher>();
    private long id;

    public SubjectForm(long id, String subjectName, String subjectDescription, UserTER userInCharge) {
        this.subjectName = subjectName;
        this.subjectDescription = subjectDescription;
        this.userInCharge = userInCharge;
        this.id = id;
    }

    public SubjectForm(long id, String subjectName, String subjectDescription, UserTER userInCharge, List<Teacher> secondaryInCharge) {
        this.subjectName = subjectName;
        this.subjectDescription = subjectDescription;
        this.userInCharge = userInCharge;
        this.secondaryInCharge = secondaryInCharge;
        this.id = id;
    }

    public SubjectForm() {}

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectDescription() {
        return subjectDescription;
    }

    public void setSubjectDescription(String subjectDescription) {
        this.subjectDescription = subjectDescription;
    }

    public UserTER getUserInCharge() {
        return userInCharge;
    }

    public void setUserInCharge(UserTER userInCharge) {
        this.userInCharge = userInCharge;
    }

    public List<Teacher> getSecondaryInCharge(){
        return secondaryInCharge;
    }

    public void setSecondaryInCharge(List<Teacher> secondaryInCharge){
        this.secondaryInCharge = secondaryInCharge;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
