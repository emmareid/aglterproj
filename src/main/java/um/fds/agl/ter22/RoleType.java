package um.fds.agl.ter22;

public class RoleType {
    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String ROLE_TEACHER = "ROLE_TEACHER";
    public static final String ROLE_STUDENT = "ROLE_STUDENT";

    private RoleType(){}
}
